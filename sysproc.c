#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;
  int signum;
  if(argint(0, &pid) < 0 || argint(1, &signum) < 0 || signum < 0 || signum > 31){
    cprintf("sys_kill: could not fetch args properly\n");
    return -1;
  }
  return kill(pid, signum);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed  || (((myproc()->signals_mask & SIGKILL) != SIGKILL) && (myproc()->pending_signals & SIGKILL) == SIGKILL)){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

int sys_sigprocmask(void){
  int sigmask;
  argint(0, &sigmask);
  return sigprocmask((uint)sigmask);
}

sighandler_t sys_signal(void){
  int signum;
  int handler;
  // cprintf("*in sys_signal====================\n");
  if (argint(0, &signum) < 0 || argint(1, &handler) < 0 || signum < 0 || signum > 31){
    cprintf("problem in sys_signal\n");
    return (sighandler_t)-2;
  }
    // cprintf("in sysproc::sys_signal--handler == %p\n", handler);
    // cprintf("in sysproc::sys_signal--signum == %d\n", signum);
  return signal(signum, (sighandler_t)handler);
}

void sys_sigret(void){
  // cprintf("calling sigret\n");
  return sigret();
}