#include "types.h"
#include "stat.h"
#include "user.h"


void sig_handler1(int signum);
void sig_handler2(int signum);
// son looping during the whole time
void three_user_signals(); // father sends 3 signals. check also SIGSTOP, SIGKILL and SIGCONT
void signal_bomb(); // father sends 32 signals to son
void override_signal_handler();

static inline void rep_nop(void) {
  asm volatile("rep; nop" ::: "memory");
}

void inf_loop(void){
  for(;;){
      rep_nop();
    }
}

void override_signal_handler(){
    printf(1, "override signal handler test\n");
    int pid;
    signal(20, (void *)&sig_handler1);
    sleep(5);
    signal(20, (void *)&sig_handler2);
    sleep(5);
    if ((pid = fork()) == 0){
        printf(1, "son looping\n");
        inf_loop();
        printf(1, "finishing\n");
    }
    else{
        int pid1 = pid;
        if (!kill(pid1, 20)){
            printf(2, "problem in kill 20\n");
            exit();
        }
        sleep(5);
        printf(1, "father: killing proc %d\n", pid1);
        if (!kill(pid1, 9)){
            printf(2, "problem in kill 20\n");
            exit();
        }
        wait();
        printf(1, "override signal handler test ends\n");
    }

}

void three_user_signals(){
  int pid;
  // printf(1, "sig_handler1 addres is: %p\n", &sig_handler1);
  signal(20, (void *)&sig_handler1);
  sleep(5);
  signal(21, (void *)&sig_handler1);
  sleep(5);
  signal(22, (void *)&sig_handler1);
  sleep(5);
  // signal(22, (void *)1);
  sleep(5);
  if ((pid = fork()) == 0){
    // for(i = 0 ; i < 32 ; i++){
    //   if((i != 19) && (i != 17) && (i != 9)){
    //     printf(1, "%d ",i);
    //     signal(i, &sig_lol_handler);
    //   }
    //     // printf(1, "signum %d is set\n", i);
    // }
    printf(1, "son looping\n");
    inf_loop();
    printf(1, "finishing\n");
  }
  else {
    int j = 0;
    int pid1 = pid;
    printf(1, "stopping proc number %d sending signal 17\n", pid1);
    j = kill(pid1, 17);
    if (j != 0){
      printf(1, "signalsanity: problem in kill 17\n");
      exit();
    }
    printf(1, "father sleeping\n");
    sleep(10);
    printf(1, "sending sig number 20-22 to proc number %d\n", pid1);
    j = kill(pid1, 20);
    sleep(3);
    if (j != 0){
      printf(1, "problem in kill 20, kill retval = %d\n", j);
      exit();
    }
    sleep(100);
    j = kill(pid1, 21);
    if (j != 0){
      printf(1, "problem in kill 21, kill retval = %d\n", j);
      exit();
    }
    sleep(3);
    j = kill(pid1, 22);
    if (j != 0){
      printf(1, "problem in kill 22, kill retval = %d\n", j);
      exit();
    }
    printf(1, "resuming proc number %d\n", pid1);
    kill(pid1, 19);
    sleep(1000);
    printf(1, "killing proc number %d\n", pid1);
    kill(pid1, 9);
    printf(1,"waiting for son...\n");
    wait();
    printf(1, "child killed\nchild pid1: %d\n", pid1);
  }
}

void signal_bomb(){
  printf(1, "signal bomb test start\n");
    int i = 0; int pid;
    for (i = 0 ; i < 32 ; i++){
        if(i != 19 && i != 17 && i != 9)
          signal(i, &sig_handler2);
    }
    if ((pid = fork()) == 0){
        printf(1, "son looping\n");
        inf_loop();
        printf(1, "finishing\n");
    }
    else{
        sleep(1000);
        for(i = 0 ; i < 32 ; i++){
          if(i != 19 && i != 17 && i != 9){
            printf(1, "'%d' ",i);
            kill(pid, i);
            sleep(2); // giving time to send the signal
          }
        }
        sleep(10);
        printf(1, "killing proc no %d\n", pid);
        kill(pid, 9);
        wait();
        printf(1, "signal bomb test end\n");

    }
}


int
main(int argc, char **argv)
{
  three_user_signals();
  signal_bomb();
  override_signal_handler();
  exit();
}


void sig_handler1(int signum){
  printf(1, "\nhandling signal\n"/*number: '%d'!\n, signum*/);
  printf(1, "sig_handler1: signum handled: %d\n", signum);
  printf(1, "\nfinishing handling signal\n"/*number: '%d'!\n, signum*/);
}

void sig_handler2(int signum){
  printf(1, "\nhandling signal\n"/*number: '%d'!\n, signum*/);
  printf(1, "sig_handler2: signum handled: %d\n", signum);
  printf(1, "finishing handling signal\n"/*number: '%d'!\n, signum*/);
}