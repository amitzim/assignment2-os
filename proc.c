#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "proc.h"
#include "spinlock.h"

struct {
  struct spinlock lock;
  struct proc proc[NPROC];
} ptable;

static struct proc *initproc;

int nextpid = 1;
extern void forkret(void);
extern void trapret(void);
extern void check_all_signals();
extern uint sigret_end;
extern uint sigret_start;

asm (".globl sigret_start\n"
	".globl sigret_end\n"
	"sigret_start:\n\t"
	"movl $24, %eax\n\t"
	"int $64\n"
	"sigret_end:");

static void wakeup1(void *chan);

sighandler_t signal(int signum, sighandler_t handler){
  struct proc *p;
  p = myproc();
  sighandler_t old = p->signal_handlers[signum];
  p->signal_handlers[signum] = handler;
  // cprintf("\n*****in signal -> handler: %p\n", handler);
  // cprintf("\n*****in signal -> p->signal_handlers[signum]: %p\n", p->signal_handlers[signum]);
  return old;
}

uint sigprocmask(uint sigprocmask){
  struct proc *p;
  p = myproc();
  uint oldsigmask = p->signals_mask;
  p->signals_mask = sigprocmask;
  return oldsigmask;
}

void handle_stop(void){
  while (1){
  	// myproc()->handling_signals_now = 1;
    cprintf("BUSYWAIT ");
    if((myproc()->pending_signals & (1 << SIGCONT)) == (1 << SIGCONT)){
      cprintf("GOT CONT SIGNAL!\n");
      myproc()->pending_signals = myproc()->pending_signals ^ (1 << SIGCONT);
      // myproc()->handling_signals_now = 0;
      break;
    }
    yield();
      
  }
}

void sigret(){
    memmove(myproc()->tf, &myproc()->user_trapframe_backup, sizeof(struct trapframebackup));
    myproc()->handling_signals_now = 0;
}

void handle_kill(struct proc *p){
  cprintf("in handle_kill\n");
  // p->killed = 1;
  cas(&p->killed, 0, 1);
  cas(&p->state, SLEEPING, RUNNABLE);
  //   cprintf("handle_kill: changing p->state from SLEEPING to RUNNABLE\n");
  //   cprintf("p->state: %d\n", p->state);
  // }
  // if(p->state == SLEEPING)
  //   p->state = RUNNABLE;
  // p->handling_signals_now = 0;
  cprintf("end of handle_kill\n");
}

void case_user_defined_sighandler(int i, int shifted_index, struct proc *p){
  p->pending_signals = p->pending_signals ^ shifted_index;
  cprintf("signal handler pointer is: %p\n", p->signal_handlers[i]);
  if(p->signal_handlers[i] == (sighandler_t)SIG_DFL){ // act like kill
    if(p->signal_handlers[SIGKILL] == (sighandler_t)SIG_DFL){
      cprintf("not kill, stop or cont but default.. handling like kill\n");
      handle_kill(p);
      myproc()->handling_signals_now = 0;
      return;
    }
    cprintf("kill was modified by user\n");
    i = SIGKILL;
  }
  if ((p->tf->cs & 3) != DPL_USER)
    return;
  cprintf("handler is user defined. modifing the stack and eip\nsignum: %d\n",i);
  // cprintf("signal_handlers[i] = %p\n", p->signal_handlers[i]);
  p->tf->eip = (uint)p->signal_handlers[i]; //eip points to the beginning of the user signal handlre func
  p->signal_handlers[i] = (void *)SIG_DFL;
  // p->tf->esp -= &sigret_end - &sigret_start;
  p->tf->esp -= (uint)&sigret_end - (uint)&sigret_start;
  uint ret_addr_injected_code = (uint)p->tf->esp;
  // cprintf("uint: %d\n", sizeof(uint));
  // cprintf("sigret_start: %p\n", sigret_start);
  // cprintf("sigret_end: %p\n", sigret_end);
  // cprintf("&sigret_end - &sigret_start: %d\n", (uint)&sigret_end - (uint)&sigret_start);
  memmove((void *)p->tf->esp, &sigret_start, (uint)&sigret_end - (uint)&sigret_start);  // memmove((void *)p->tf->esp, (void *)i, sizeof(int));
  p->tf->esp -= sizeof(int);
  memmove((void *)p->tf->esp, &i, sizeof(int));  // inject signum to stack
  p->tf->esp -= sizeof(uint);
  memmove((void *)p->tf->esp, &ret_addr_injected_code, sizeof(uint));  // memmove((void *)p->tf->esp, (void *)i, sizeof(int));
  // p->tf->esp = ret_addr_injected_code;
}

int handle_stop_cont_kill(int i, int shifted_index, struct proc *p){
  if(p->signal_handlers[i] == (sighandler_t)SIG_DFL){
    p->pending_signals = p->pending_signals ^ shifted_index;
    switch (i){
      case SIGKILL:
      	cprintf("handle_stop_cont_kill: got SIGKILL\n");
        handle_kill(p);
        break;
      case SIGSTOP:
      	cprintf("handle_stop_cont_kill: got SIGSTOP\n");
        handle_stop();
        i = 0;
        break;
      case SIGCONT:
      	cprintf("handle_stop_cont_kill: got SIGCONT\n");
        // p->handling_signals_now = 0;
        //nothing happens
        break;
    }
  }
  else {
    case_user_defined_sighandler(i, shifted_index, p);
  }
  return i;
}


//Checks all signals of current process
void check_all_signals(){
  struct proc *p;
  p = myproc();
  if(p == 0)
    return;
  // already handling signals
  if (p->handling_signals_now == 1){
  	// cprintf("signals are blocked\n");
    return;
  }
  // now handling signals
  p->handling_signals_now = 1;
  memmove(&p->user_trapframe_backup, p->tf, sizeof(struct trapframebackup));

  int i = 0;
  int shifted_index = 0;

  if(p->pending_signals != 0){
    cprintf("checking signals\n");
    cprintf("pending_signals: %d\n", (uint)p->pending_signals);
  }
  // iterates over all signals
  for(i = 0 ; i < 32 ; i++){
    shifted_index = 1 << i;
    // signal number i is pending and not blocked by mask
    if(((p->pending_signals & shifted_index) == shifted_index) && ((p->signals_mask & shifted_index) != shifted_index)){

      if(p->signal_handlers[i] == (sighandler_t)SIG_IGN){
        cprintf("signal handler for signum no '%d' set to be ignored\n", i);
        p->pending_signals = p->pending_signals ^ shifted_index;
        continue;
      }
      if(i == SIGCONT || i == SIGSTOP || i == SIGKILL){
        cprintf("signal no '%d' is kill, cont or stop\n", i);
        i = handle_stop_cont_kill(i, shifted_index, p);
        // p->handling_signals_now = 0;
        // return;
        // break;
        continue;
      }
      cprintf("signal no '%d' user defined handler\n", i);
      case_user_defined_sighandler(i, shifted_index, p);
      return;
    }
  }
  p->handling_signals_now = 0;
  // cprintf("end of check_all_signals\n");
}


void
pinit(void)
{
  initlock(&ptable.lock, "ptable");
}

// Must be called with interrupts disabled
int
cpuid() {
  return mycpu()-cpus;
}

// Must be called with interrupts disabled to avoid the caller being
// rescheduled between reading lapicid and running through the loop.
struct cpu*
mycpu(void)
{
  int apicid, i;
  
  if(readeflags()&FL_IF)
    panic("mycpu called with interrupts enabled\n");
  
  apicid = lapicid();
  // APIC IDs are not guaranteed to be contiguous. Maybe we should have
  // a reverse map, or reserve a register to store &cpus[i].
  for (i = 0; i < ncpu; ++i) {
    if (cpus[i].apicid == apicid)
      return &cpus[i];
  }
  panic("unknown apicid\n");
}

// Disable interrupts so that we are not rescheduled
// while reading proc from the cpu structure
struct proc*
myproc(void) {
  struct cpu *c;
  struct proc *p;
  pushcli();
  c = mycpu();
  p = c->proc;
  popcli();
  return p;
}

int 
allocpid(void) 
{

 int pid ;
 do{
	 pid = nextpid; 
 }
  while(cas(&nextpid,pid,pid + 1) == 0);

  return pid + 1;
}



//PAGEBREAK: 32
// Look in the process table for an UNUSED proc.
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
  struct proc *p;
  char *sp;

  // acquire(&ptable.lock);

  pushcli();
  do{
	for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
	  if(p->state == UNUSED)
	    break;
		// Required for forktest in usertests. 
		// if ptable is full, fork has to fail gracefully
	    if (p == &ptable.proc[NPROC]) {
	      popcli();
	      return 0; // ptable is full
	    }
  }
  while (cas(&p->state, UNUSED, EMBRYO) == 0);

  // release(&ptable.lock);
  // return 0;
  popcli();
// found:
//   p->state = EMBRYO;
//   release(&ptable.lock);
  
  p->pid = allocpid();

  // Setting all signal handlers to DEFUALT
  int i = 0;
  for(i = 0 ; i < 32 ; i++){
    p->signal_handlers[i] = (sighandler_t)SIG_DFL;
  }

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
    p->state = UNUSED;
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;

  // Leave room for trap frame.
  sp -= sizeof *p->tf;
  p->tf = (struct trapframe*)sp;

  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
  p->context->eip = (uint)forkret;

  return p;
}

//PAGEBREAK: 32
// Set up first user process.
void
userinit(void)
{
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];

  p = allocproc();
  
  initproc = p;
  if((p->pgdir = setupkvm()) == 0)
    panic("userinit: out of memory?");
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
  p->sz = PGSIZE;
  memset(p->tf, 0, sizeof(*p->tf));
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
  p->tf->es = p->tf->ds;
  p->tf->ss = p->tf->ds;
  p->tf->eflags = FL_IF;
  p->tf->esp = PGSIZE;
  p->tf->eip = 0;  // beginning of initcode.S

  safestrcpy(p->name, "initcode", sizeof(p->name));
  p->cwd = namei("/");

  // this assignment to p->state lets other cores
  // run this process. the acquire forces the above
  // writes to be visible, and the lock is also needed
  // because the assignment might not be atomic.
  // pushcli();
  // acquire(&ptable.lock);

  if(!cas(&p->state, EMBRYO, RUNNABLE)){
  	panic("userinit: tranistion failed from UNUSED to RUNNABLE");
  }
  // p->state = RUNNABLE;

  // popcli();
  // release(&ptable.lock);
}

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
  uint sz;
  struct proc *curproc = myproc();

  sz = curproc->sz;
  if(n > 0){
    if((sz = allocuvm(curproc->pgdir, sz, sz + n)) == 0)
      return -1;
  } else if(n < 0){
    if((sz = deallocuvm(curproc->pgdir, sz, sz + n)) == 0)
      return -1;
  }
  curproc->sz = sz;
  switchuvm(curproc);
  return 0;
}

// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
  int i, pid;
  struct proc *np;
  struct proc *curproc = myproc();

  // Allocate process.
  if((np = allocproc()) == 0){
    return -1;
  }

  // Copy process state from proc.
  if((np->pgdir = copyuvm(curproc->pgdir, curproc->sz)) == 0){
    kfree(np->kstack);
    np->kstack = 0;
    np->state = UNUSED;
    return -1;
  }
  np->sz = curproc->sz;
  np->parent = curproc;
  *np->tf = *curproc->tf;


  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
    if(curproc->ofile[i])
      np->ofile[i] = filedup(curproc->ofile[i]);
  np->cwd = idup(curproc->cwd);

  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
  
  for(i = 0 ; i < 32 ; i++){
    np->signal_handlers[i] = curproc->signal_handlers[i];
  }
  np->signals_mask = curproc->signals_mask;
  np->pending_signals = 0;
  np->handling_signals_now = 0;
  pid = np->pid;

  // acquire(&ptable.lock);
  // pushcli();
  // cprintf("in fork: np->state is: %d\n", np->state);
  if(!cas(&np->state, EMBRYO, RUNNABLE)){
  	panic("fork: failed tranistion from to UNUSED to RUNNABLE");
  }
  // np->state = RUNNABLE;
  // popcli();
  // release(&ptable.lock);

  return pid;
}

// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void
exit(void)
{
  struct proc *curproc = myproc();
  struct proc *p;
  int fd;

  if(curproc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(curproc->ofile[fd]){
      fileclose(curproc->ofile[fd]);
      curproc->ofile[fd] = 0;
    }
  }

  begin_op();
  iput(curproc->cwd);
  end_op();
  curproc->cwd = 0;

  pushcli();
  // acquire(&ptable.lock);

  // Parent might be sleeping in wait().

  wakeup1(curproc->parent);
  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    // cprintf("exit: p->state = %d",p->state);
    if(p->parent == curproc){
      p->parent = initproc;
      while(p->state == -ZOMBIE);
      if(p->state == ZOMBIE)
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  // curproc->state = ZOMBIE;
  if(!cas(&curproc->state, RUNNING, -ZOMBIE))
  	panic("exit: transition from RUNNING to ZOMBIE failed");
  // cprintf("in exit: before sched\n");
  sched();
  panic("zombie exit");
}

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
wait(void)
{
  struct proc *p;
  int havekids, pid;
  struct proc *curproc = myproc();
  // cprintf("******wait*********\n");
  // acquire(&ptable.lock);
  // cprintf("wait: before the pushcli ncli : %d\n",mycpu()->ncli);
  pushcli();
  for(;;){
    if(!cas(&curproc->state, RUNNING, -SLEEPING)) {
      panic("wait: transition from RUNNING to -SLEEPING failed");
    }
    curproc->chan = curproc;
    // Scan through table looking for exited children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != curproc)
        continue;
      havekids = 1;

	  // cprintf("wait: HERE1 ");
      while(p->state == -ZOMBIE);
	  // cprintf("wait: HERE2 ");
      // while(p->state != ZOMBIE){
      //   cprintf("wait: HERE\n");
      // }
	 
      if(p->state == ZOMBIE){
         
        // Found one.
        pid = p->pid;
        kfree(p->kstack);
        p->kstack = 0;
        freevm(p->pgdir);
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        p->killed = 0;
        if (!cas(&p->state, ZOMBIE, UNUSED))
          panic("wait: zombie to unused failed");
        // p->state = UNUSED;
        // release(&ptable.lock);
        // cprintf("rawn 1\n");
        if(!cas(&curproc->state, -SLEEPING, RUNNING)) {
          panic("wait: transition from RUNNING to -SLEEPING failed");
        }
        curproc->chan = 0;
        popcli();
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || curproc->killed){
      // cprintf("rawn 2\n");
      // release(&ptable.lock);
      if(!cas(&curproc->state, -SLEEPING, RUNNING)) {
          panic("wait: transition from RUNNING to -SLEEPING failed");
      }
      curproc->chan = 0;
      popcli();
      return -1;
    }
    // cprintf("wait: p->state: %d\n", p->state);
    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    // cprintf("wait: before sched() ncli: %d\n",mycpu()->ncli);
    
    // else{
    //   cprintf("wait: pid: %d",myproc()->pid);
    // }

    sched();
    // sleep(curproc, &ptable.lock);  //DOC: wait-sleep
  }
}
//PAGEBREAK: 42
// Per-CPU process scheduler.
// Each CPU calls scheduler() after setting itself up.
// Scheduler never returns.  It loops, doing:
//  - choose a process to run
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch beack to the scheduler.
void
scheduler(void)
{
  struct proc *p;
  struct cpu *c = mycpu();
  c->proc = 0;
  
  for(;;){
    // Enable interrupts on this processor.
    //cprintf("ad matai???\n");
    sti();

    // Loop over process table looking for process to run.
    // acquire(&ptable.lock);
    pushcli();
    // do {
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    	if((!cas(&p->state, RUNNABLE,RUNNING) /*|| !cas(&p->state, -RUNNABLE,RUNNING)*/)){
    		continue;
    	}
	   //  	else{
				// // cprintf("scheduler: p->state: %d\npid: %d\n", p->state, p->pid);
    //     }
        // if(p->state == RUNNABLE)
        //   break;
    
  // }
  // while(cas(&p->state, RUNNABLE,RUNNING) == 0);
  // Switch to chosen process.  It is the process's job
  // to release ptable.lock and then reacquire it
  // before jumping back to us.
    c->proc = p;
    switchuvm(p);
    // p->state = RUNNING;

    swtch(&(c->scheduler), p->context);
  	switchkvm();

  	// Process is done running for now.
  	// It should have changed its p->state before coming back.
  	c->proc = 0;
    switch((int)p->state){
      case -ZOMBIE:
        if(!cas(&p->state, -ZOMBIE, ZOMBIE)){
            panic("scheduler: failed transition from -ZOMBIE to ZOMBIE");}
        else{
            // cprintf("in case ZOMBIE. parent is %s", p->parent->name);
            wakeup(p->parent);
         }
        break;
      case -RUNNABLE:
        if(!cas(&p->state, -RUNNABLE, RUNNABLE))
          panic("scheduler: failed transition from -RUNNABLE to RUNNABLE");
      //    }
      //    else{
      //     // cprintf("in case RUNNABLE\n");
      //     // wakeup1(p->parent);
      //    }
        break;
      case -SLEEPING:
        if(!cas(&p->state, -SLEEPING, SLEEPING))
          panic("scheduler: failed transition from -SLEEPING to SLEEPING");
         else{
          // cprintf("in case sleeping\n");
          // if (cas(&p->killed, 1, 1)){
          //   // p->state = RUNNABLE;
          //     cas(&p->state, SLEEPING, RUNNABLE);
          // }
         }
        break;
      default:
        break;

    }
    
	}
    
  popcli();
  // release(&ptable.lock);

  }
}

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state. Saves and restores
// intena because intena is a property of this
// kernel thread, not this CPU. It should
// be proc->intena and proc->ncli, but that would
// break in the few places where a lock is held but
// there's no process.
void
sched(void)
{
  int intena;
  struct proc *p = myproc();

  // if(!holding(&ptable.lock))
  //   panic("sched ptable.lock");
  // cprintf("mycpu()->ncli = %d\n", mycpu()->ncli);
  if(mycpu()->ncli != 1)
    panic("sched locks");
  if(p->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = mycpu()->intena;
  swtch(&p->context, mycpu()->scheduler);
  mycpu()->intena = intena;
}

// Give up the CPU for one scheduling round.
void
yield(void)
{
	// cprintf("pid %d in yield\n", myproc()->pid);
 //acquire(&ptable.lock);  //DOC: yieldlock
	pushcli();
	if(!cas(&myproc()->state,RUNNING,-RUNNABLE))
		panic("yield: CAS from RUNNING to -RUNNABLE failed");
	// cprintf("before sched in yield\n");
	sched(); 
	// cprintf("after sched in yield\n");
	// if(!cas(&myproc()->state,-RUNNABLE,RUNNABLE)){
	//   panic("CAS from -RUNNABLE to RUNNABLE failed! in yield()");
	// }
  // cprintf("yield: myproc()->state: %d\n", myproc()->state);
	popcli();
	// cprintf("end of yield\n");

}
// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  // release(&ptable.lock);
  popcli();

  if (first) {
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot
    // be run from main().
    first = 0;
    iinit(ROOTDEV);
    initlog(ROOTDEV);
  }

  // Return to "caller", actually trapret (see allocproc).
}


// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void
sleep(void *chan, struct spinlock *lk)
{
  // cprintf("in sleep\n");
  struct proc *p = myproc();
  if(p == 0)
    panic("sleep");

  if(lk == 0)
    panic("sleep without lk");

  // Must acquire ptable.lock in order to
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.

  pushcli();
  p->chan = chan;
  if(!cas(&p->state, RUNNING, -SLEEPING))
  	panic("sleep: status fron RUNNING to -SLEEPING failed");
  
  if(lk != &ptable.lock){  //DOC: sleeplock0
    // acquire(&ptable.lock);  //DOC: sleeplock1
    // pushcli();
    release(lk);
  }

  // Go to sleep.
	// cprintf("in sleep: before sched\n");
	sched();
	// cprintf("in sleep: after sched\n");
  
  // Reacquire original lock.
  if(lk != &ptable.lock){  //DOC: sleeplock2
    // release(&ptable.lock);
    // popcli();
    acquire(lk);
  }
	popcli();
}

//PAGEBREAK!
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
	struct proc *p;
  
	for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    	if((p->state == SLEEPING || p->state == -SLEEPING) && p->chan == chan){
        // cprintf("!!!!!!!!!!!!!!!!!!!!!!!\n");
    		while (p->state == -SLEEPING){
          // cprintf("wakeup1: busywait 1\n");
        	}
	        while(!cas(&p->state, SLEEPING, -RUNNABLE)){
	          // cprintf("wakeup1: busywait 2\n");
	        }
	        // cprintf("tidy up\n");
	        p->chan = 0;
	        while(!cas(&p->state, -RUNNABLE, RUNNABLE)){
	          // cprintf("wakeup1: busywait 3\n");
        	}

	// panic("wakeup1: transition from SLEEPING to -RUNNABLE failed");
		}
  	}
}

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
	pushcli();
  // acquire(&ptable.lock);
	wakeup1(chan);
	popcli();
  // release(&ptable.lock);
}

// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid, int signum)
{
  struct proc *p;

  // acquire(&ptable.lock);
  pushcli();
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid){
        cprintf("kill: signum is '%d' pid is: '%d'\n", signum, pid);
        int bit_to_change = 1 << signum;
        while(!cas(&p->pending_signals, p->pending_signals, p->pending_signals | bit_to_change));
        // panic("kill: cant update signal");
        if(signum == SIGKILL){
        while(!cas(&p->killed, p->killed, 1));
        // p->killed = 1;
        // cas(&p->state, SLEEPING, RUNNABLE);
        // p->state = RUNNABLE;
        // while(p->state == -SLEEPING){
        //   cprintf("kill: HERE1\n");
        // }
        // while(!cas(&p->state, SLEEPING, RUNNABLE)){
        //   cprintf("kill: p->state: %d\n", p->state);
          // panic("kill: cant wakeup sleeping process that got killed");
        // }
      }
      // Wake process from sleep if necessary.
      // release(&ptable.lock);
      popcli();
      return 0;
    }
  }
  popcli();
  // release(&ptable.lock);
  cprintf("kill: kill failed, probably didnt find proc with pid: %d\n", pid);
  return -1;
}

//PAGEBREAK: 36
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
  static char *states[] = {
  [UNUSED]    "unused",
  [EMBRYO]    "embryo",
  [SLEEPING]  "sleep ",
  [RUNNABLE]  "runble",
  [RUNNING]   "run   ",
  [ZOMBIE]    "zombie"
  };
  int i;
  struct proc *p;
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
      state = states[p->state];
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }
}
